#!perl -T
use 5.006;
use strict;
use warnings;
use Test::More;

plan tests => 1;

BEGIN {
    use_ok( 'Whisk::SDK' ) || print "Bail out!\n";
}

diag( "Testing Whisk::SDK $Whisk::SDK::VERSION, Perl $], $^X" );
